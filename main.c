#include <stdio.h>
#include <math.h>
const int array1[] = {1, 2, 3, 4, 10};
const int array2[] = {5, 6, 7, 8, 15};

int scalar_sum(const int* array1, const int* array2, size_t size_of_array){
	int sum = 0;
	size_t i;
	for(i=0; i< size_of_array; i++)
		sum+=array1[i]*array2[i];
	return sum;
}

int is_prime(unsigned long n){
	unsigned long i;
    for (i = 2; i <= n/2; i++){
        if (!(n % i)) return 0;
    }
    return 1;
}

int main(int args, char** argv) {
	size_t i;
	size_t size_of_array1 = sizeof(array1) / sizeof(int);
	size_t size_of_array2 = sizeof(array2) / sizeof(int);
	size_t min_size;
	unsigned long n = 0;
	if(size_of_array1 < size_of_array2) min_size = size_of_array1;
	else min_size = size_of_array2;
	printf("Array1: ");
	for(i=0; i<size_of_array1; i++) {
		printf("%d ", array1[i]);
	}
	
	printf("\nArray2: ");
	for(i=0; i<size_of_array2; i++) {
		printf("%d ", array2[i]);
	}
	printf("\nScalar sum = %d\n", scalar_sum(array1, array2, min_size));

	for (;;){
	printf("Input unsigned long n: ");
	scanf("%lu", &n);
	if((long)n <= 1)
		printf("No, %ld isn't prime\n ", n);	
	else if (n > 4294967295) printf("Too big number!\n");
	else printf(is_prime(n) ? "Yes, %lu is prime\n" : "No, %ld isn't prime\n", n);
	}
	return 0;
}




